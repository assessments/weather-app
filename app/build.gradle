plugins {
    id 'com.android.application'
    id 'kotlin-android'
    id 'kotlin-kapt'
    id 'dagger.hilt.android.plugin'
    id 'kotlin-android-extensions'
}

android {
    compileSdk 30
    buildToolsVersion "30.0.3"

    defaultConfig {
        applicationId "com.dvt.weatherapp"
        minSdk 21
        targetSdk 29
        versionCode 1
        versionName "1.0"

        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        def WEATHER_API_KEY = "7d0a73854aa8322717072c3a2d6575d1"
        def BASE_URL = "https://api.openweathermap.org/data/2.5/"

        debug {
            buildConfigField 'String', 'BASE_URL', "\"${BASE_URL}\""
            buildConfigField 'String', 'WEATHER_API_KEY', "\"${WEATHER_API_KEY}\""
        }
        release {
            buildConfigField 'String', 'BASE_URL', "\"${BASE_URL}\""
            buildConfigField 'String', 'WEATHER_API_KEY', "\"${WEATHER_API_KEY}\""
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
    dataBinding {
        enabled true
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = '1.8'
    }
}

dependencies {

    implementation "org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version"
    implementation 'androidx.core:core-ktx:1.5.0'
    implementation 'androidx.appcompat:appcompat:1.3.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    implementation 'android.arch.lifecycle:extensions:1.1.1'

    // Material Design
    implementation "com.google.android.material:material:$material_version"

    // Architectural Components
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version"

    // Coroutines
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutine_version"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutine_version"

    // Coroutine Lifecycle Scopes
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-runtime-ktx:$lifecycle_version"

    // KTX for viewModels()
    implementation "androidx.activity:activity-ktx:1.2.3"
    implementation 'androidx.fragment:fragment-ktx:1.3.5'

    //Dagger - Hilt
    implementation "com.google.dagger:hilt-android:$hilt_version"
    kapt "com.google.dagger:hilt-android-compiler:$hilt_version"

    // Play Services
    implementation 'com.google.android.gms:play-services-maps:17.0.1'
    implementation 'com.google.android.gms:play-services-location:18.0.0'

    // Retrofit
    implementation "com.squareup.retrofit2:retrofit:$retrofit_version"
    implementation "com.squareup.retrofit2:converter-gson:$retrofit_version"
    implementation "com.squareup.okhttp3:logging-interceptor:4.5.0"

    // Navigation Component
    implementation "androidx.navigation:navigation-fragment-ktx:$nav_version"
    implementation "androidx.navigation:navigation-ui-ktx:$nav_version"

    // Glide
    implementation ("com.github.bumptech.glide:glide:$glide_version") {
        exclude group: "com.android.support"
    }

    // Room
    implementation "androidx.room:room-ktx:$room_version"
    kapt "androidx.room:room-compiler:2.3.0"

    // Flexbox
    implementation 'com.google.android.flexbox:flexbox:3.0.0'

    // Easy Permissions
    implementation "pub.devrel:easypermissions:$easypermission_version"

    testImplementation 'junit:junit:4.13.2'
    androidTestImplementation 'androidx.test.ext:junit:1.1.2'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.3.0'
}